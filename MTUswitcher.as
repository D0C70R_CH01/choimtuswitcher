﻿package
{
	/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

	import flash.display.MovieClip;
	import flash.desktop.NativeProcess;
	import flash.filesystem.File;
	import flash.events.ProgressEvent;
	import flash.utils.IDataInput;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.NativeWindow;
	import flash.desktop.NativeProcessStartupInfo;
	//import 끗

	public class MTUswitcher extends MovieClip
	{
		private const FILE: File = new File("c:\\windows\\system32\\cmd.exe");
		//상수선언 끗

		private var _nw: NativeWindow = stage.nativeWindow;
		//변수선언 끗

		public function MTUswitcher(): void
		{
			this.txtLog.text = "Copyright (c) 2019 최박사 | DoctorChoi";
			
			this.btnDrag.addEventListener(MouseEvent.MOUSE_DOWN, this._fdrag);
			this.btnQuit.addEventListener(MouseEvent.CLICK, this._fquit);
			this.btnSet.addEventListener(MouseEvent.CLICK, this._fset);
			this.btn56.addEventListener(MouseEvent.CLICK, this._f56);
			this.btn1500.addEventListener(MouseEvent.CLICK, this._f1500);
			//이벤트 생성 끗
		}
		//생성자 끗
		
		private function _fswitchMTU(_network: String, _value: int): void
		{
			var _np: NativeProcess = new NativeProcess();
			var _info: NativeProcessStartupInfo = new NativeProcessStartupInfo();
			var _v: Vector.<String> = new Vector.<String>;
			//변수선언 끗

			_v.push('cmd /c start netsh interface ipv4 set subinterface "' + _network + '" mtu=' + _value + ' store=persistent');
			//명령어 설정
			
			_info.arguments = _v;
			_info.executable = FILE;
			//시작 정보 설정

			_np.start(_info);
			//시작
		}
		//MTU값 건드리는 함수 끗

		private function _fdrag(e: MouseEvent): void
		{
			this._nw.startMove();
		}
		//창 드래그 함수 끗

		private function _fquit(e: MouseEvent): void
		{
			this.txtLog.text = "ㅂㅂ";

			_fswitchMTU(this.networkName.text, 1500);

			_nw.close();
		}
		//MTU값 원래대로 돌리고 창 닫는 함수 끗
		
		private function _fset(e: MouseEvent): void
		{
			if(this.networkName.text == "")
			{
				this.txtLog.text = "네트워크 이름을 입력해주세요.";
			}
			else if(this.mtuValue.text == "" || int(this.mtuValue.text) < 56 || int(this.mtuValue.text) > 1500)
			{
				this.txtLog.text = "56~ 1500 사이의 수를 입력해주세요.";
			}
			else
			{
				_fswitchMTU(this.networkName.text, int(this.mtuValue.text));
			}
		}
		//설정 버튼 함수 끗

		private function _f56(e: MouseEvent): void
		{
			if(this.networkName.text == "")
			{
				this.txtLog.text = "네트워크 이름을 입력해주세요.";
			}
			else
			{
				_fswitchMTU(this.networkName.text, 56);
			}
		}
		//MTU 56으로 설정하는 버튼 함수 끗

		private function _f1500(e: MouseEvent): void
		{
			if(this.networkName.text == "")
			{
				this.txtLog.text = "네트워크 이름을 입력해주세요.";
			}
			else
			{
				_fswitchMTU(this.networkName.text, 1500);
			}
		}
		//MTU 56으로 설정하는 버튼 함수 끗
	}
}